f= @(x,y) x/y;  %y'=f
h=0.1;  %krok calkowania
xo=0;   %warunek poczatkowy
yo=2;   %warunek poczatkowy
n=5;   %ilosc krokow 
y=udeuler(h,f,xo,n,yo)
t=(0:h:h*n);

plot(t,y);
title('Udoskonalony Euler/ODE45')
hold on
[t2,y2]=ode45(f,[xo,h*n],yo) %przedzial od xo do h*n
plot(t2,y2);
legend('udeuler','ODE45')


