function ywyj = udeuler(h,f,xo,n,yo) %h-krok ckowania, f-rownanie, n-ilosc krokow x0-pierwszy x, yo= y(x0)

       y=yo;
       ywyj=y;
       x=xo;

       for i=1:n       %i- numer 
          dy=h*(f(x,y)+f(x+h,y+f(x,y)*h))/2;
            y=y+dy;
            x=x+h;
           ywyj=[ywyj;y];  
       end  
end

